# CHIP-2022-01 Detached Signatures for Bitcoin Cash

        Title: Detached Signatures for Bitcoin Cash
        First Submission Date: 2022-02-12
        Owners: bitcoincashautist (ac-A60AB5450353F40E)
                {imaginary_username?}
                {Jason Dreyzehner?}
        Type: Technical
        Layers: Consensus
        Status: DRAFT
        Current Version: 0.1
        Last Edit Date: 2022-02-12

## Contents

1. [Summary](#summary)
2. [Deployment](#deployment)
3. [Benefits](#benefits)
4. [Technical Description](#technical-description)
5. [Specification](#specification)
6. [Security Considerations](#security-considerations)
7. [Implementations](#implementations)
8. [Test Cases](#test-cases)
9. [Activation Costs](#activation-costs)
10. [Ongoing Costs](#ongoing-costs)
11. [Costs and Benefits Summary](#costs-and-benefits-summary)
12. [Risk Assessment](#risk-assessment)
13. [Evaluation of Alternatives](#evaluation-of-alternatives)
14. [Discussions](#discussions)
15. [Statements](#statements)
16. [Changelog](#changelog)
17. [Copyright](#copyright)
18. [Credits](#credits)

## Summary

[[Back to Contents](#contents)]

This proposal describes an upgrade to Bitcoin Cash peer-to-peer electronic cash system that will allow Bitcoin Cash signatures to sign entire transaction contents except signatures themselves, in effect comprehensively solving transaction malleability.

This will be accomplished by extending transaction inputs with an optional field that would contain an array of signatures detached from the input unlocking script.
Signature opcodes will be updated to optionally read the stack item as a reference to the detached signature instead as raw signature.

## Deployment

[[Back to Contents](#contents)]

This proposal targets May 2023 activation.

It complements another two proposals, part of "PMv3+" upgrade strategy:

- [CHIP-2021-02 Unforgeable Groups for Bitcoin Cash](#);
- [CHIP-2022-02 Detached Proofs for Bitcoin Cash](#).

## Benefits

[[Back to Contents](#contents)]

No available signature type can cover the contents of unlocking bytecode.
Contract developers must carefully design contracts to individually validate each pushed value and prevent malicious transaction malleation.

With the proposed "detached signatures" feature it becomes straightforward to design and validate secure, non-malleable contracts.
Existing contracts can be simplified, reducing both byte size and operation count.

## Technical Description

[[Back to Contents](#contents)]

This proposal is a spin-off from [CHIP-2021-01-PMv3: Version 3 Transaction Format] to enable a less complex variant of the "detached signature" feature described there, and in a way that would not break node-dependent software.

### Detached Signatures FORMAT

We will extend the input format using the non-breaking [**P**re**F**i**X**](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2021-12-23_evaluate_viability_of_transaction_format_or_id_change_EN.md#an-alternative-to-breaking-change) method:

* transaction inputs
    * input 0
        * previous output transaction hash, 32 raw bytes
        * previous output index, 4-byte uint
        * unlocking script length, [compact variable length integer](https://reference.cash/protocol/formats/variable-length-integer)
        * unlocking script
            * **PFX_SIGNATURES**, 1-byte constant `0xEF`
                * signature 0 length, [compact variable length integer](https://reference.cash/protocol/formats/variable-length-integer)
                * signature 0, variable number of raw bytes
                * ...
                * signature N length
                * signature N
            * real unlocking script, variable number of raw bytes
        * sequence number, 4-byte uint
    * ...
    * input N

We will refer to the `PFX_SIGNATURES` prefix and its signatures array as "detached signature annotation".
The annotation will be optional and may be omitted.
The annotation must come as the first input PreFiX field.
Duplicate array elements will not be allowed.

Unupgraded software, unaware of input format change, will interpret the annotation as part of the unlocking script.
As a consequence:

- Unupgraded *node* software would fork the blockchain because, from the point of view of unupgraded software, such unlocking script will be seen as starting with a disabled opcode.
- Unupgraded *non-node* software should already know how to deal with disabled opcodes found on the blockchain, so should not break when encountering them.
From its point of view `0xEF` could be some new data push opcode, followed by random data.

### Detached Signatures Consensus Rules

#### Script Virtual Machine

Script virtual machine will never see the "raw" detached signature annotation.
Signature opcodes will be updated to support 2 modes of obtaining the signature:

- Direct, if the stack item is NOT a Script number;
- Detached, if the stack item is a positive Script number.

When a detached signature is detected, consensus code will swap the reference with the related raw signature and execute the signature checking operation.
Every detached signature must be used by at least one signature opcode.

##### Introspection Opcodes

Existing introspection opcodes retrieving inputs unlocking script would continue work the same, and would return it as it is: containing references to detached signatures instead of raw signatures.
Related raw signatures would be inaccessible through introspection.

##### Signature Hash Type

There is no need to update the hash type because detached mode will be inferred from properties of the stack item.

##### Signature Preimage Format

Simply put, when a detached signature is used the preimage will be everything indicated by the hash type but with detached signature annotation(s) excluded.
Only the detached signature prefix byte and their count for the input will be included in place of detached signature annotation.

This definition supports other upgrades to transaction format that would add another input field using the same PreFiX method.
Reordering such fields would invalidate the signature because the signature hash would commit to the specific order of fields, signatures, and their number.

For the 6 valid signature hash types in Bitcoin Cash, this means:

- `SIGHASH_ALL | SIGHASH_FORKID` Signature hash commits to the entire TX except detached signatures.
- `SIGHASH_NONE | SIGHASH_FORKID` Signature hash commits to the entire input side of the TX except detached signatures.
- `SIGHASH_SINGLE | SIGHASH_FORKID` Signature hash commits to the entire input side of the TX except detached signatures, and the output with the same index.
- `SIGHASH_ALL | SIGHASH_ANYONECANPAY | SIGHASH_FORKID` Signature hash commits to its own entire input except detached signatures, and all transaction outputs.
- `SIGHASH_NONE | SIGHASH_ANYONECANPAY | SIGHASH_FORKID` Signature hash commits to its own entire input except detached signatures.
- `SIGHASH_SINGLE | SIGHASH_ANYONECANPAY | SIGHASH_FORKID` Signature hash commits to its own entire input except detached signatures, and the output with the same index.

## Specification

[[Back to Contents](#contents)]

{TODO}

## Security Considerations

[[Back to Contents](#contents)]

{TODO}

## Implementations

[[Back to Contents](#contents)]

{TODO}

## Test Cases

[[Back to Contents](#contents)]

{TODO}

## Activation Costs

[[Back to Contents](#contents)]

{TODO}

Contained to nodes.

## Ongoing Costs

[[Back to Contents](#contents)]

{TODO}

Contained to nodes.

## Costs and Benefits Summary

[[Back to Contents](#contents)]

{TODO}

## Risk Assessment

[[Back to Contents](#contents)]

{TODO}

## Evaluation of Alternatives

[[Back to Contents](#contents)]

{TODO}

## Discussions

[[Back to Contents](#contents)]

[PMv3](https://bitcoincashresearch.org/t/chip-2021-01-pmv3-version-3-transaction-format/265/53#detached-signatures-6)

## Statements

[[Back to Contents](#contents)]

## Changelog

{TODO}

[[Back to Contents](#contents)]

## Copyright

[[Back to Contents](#contents)]

To the extent possible under law, this work has waived all copyright and related or neighboring rights to this work under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

## Credits

[[Back to Contents](#contents)]

- Jason Dreyzehner, for [PMv3 proposal](https://github.com/bitjson/pmv3#chip-2021-01-pmv3-version-3-transaction-format) from which this proposal was spun off, and his extensive [malleability analysis](https://bitcoincashresearch.org/t/transaction-malleability-malfix-segwit-sighash-noinput-sighash-spendanyoutput-etc/279/9) that demonstrated the need for it.
- Calin Culianu, for coming up with the [PreFiX](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311/22) approach while discussing another proposal.

